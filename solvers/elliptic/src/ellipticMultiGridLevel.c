/*

The MIT License (MIT)

Copyright (c) 2017 Tim Warburton, Noel Chalmers, Jesse Chan, Ali Karakus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "elliptic.h"
void copy_mesh(mesh_t* target, mesh_t* source){
#ifndef OCCA_VERSION_1_0
  memcpy(target,source,sizeof(mesh_t));
#else

  target->rank = source->rank;
  target->size = source->size;

  MPI_Comm_dup(source->comm, &(target->comm));

  target->dim = source->dim;
  target->Nverts        = source->Nverts;
  target->Nfaces        = source->Nfaces;
  target->NfaceVertices = source->NfaceVertices;

  target->Nfields = source->Nfields;
  
  target->Nnodes = source->Nnodes;
  target->EX = source->EX; // coordinates of vertices for each element
  target->EY = source->EY;
  target->EZ = source->EZ;

  target->Nelements = source->Nelements;
  target->EToV = source->EToV; // element-to-vertex connectivity
  target->EToE = source->EToE; // element-to-element connectivity
  target->EToF = source->EToF; // element-to-(local)face connectivity
  target->EToP = source->EToP; // element-to-partition/process connectivity
  target->EToB = source->EToB; // element-to-boundary condition type

  target->elementInfo = source->elementInfo;

  // boundary faces
  target->NboundaryFaces = source->NboundaryFaces;
  target->boundaryInfo = source->boundaryInfo;

  // MPI halo exchange info
  target->totalHaloPairs = source->totalHaloPairs;
  target->haloElementList = source->haloElementList;
  target->NhaloPairs = source->NhaloPairs;
  target->NhaloMessages = source->NhaloMessages;

  target->haloSendRequests = source->haloSendRequests;
  target->haloRecvRequests = source->haloRecvRequests;

  target->NinternalElements = source->NinternalElements;
  target->NnotInternalElements = source->NnotInternalElements;

  target->o_haloElementList = source->o_haloElementList;
  target->o_haloBuffer      = source->o_haloBuffer;
  target->o_internalElementIds    = source->o_internalElementIds;
  target->o_notInternalElementIds = source->o_notInternalElementIds;

  // volumeGeometricFactors;
  target->Nvgeo = source->Nvgeo;
  target->vgeo = source->vgeo;
  target->o_vgeo = source->o_vgeo;

  // second order volume geometric factors
  target->Nggeo = source->Nggeo;
  target->ggeo = source->ggeo;
  target->o_ggeo = source->o_ggeo;

  target->Nsgeo = source->Nsgeo;
  target->sgeo = source->sgeo;
  target->o_sgeo = source->o_sgeo;

  // occa stuff
  target->device = source->device;

  target->defaultStream = source->defaultStream;
  target->dataStream = source->dataStream;

  target->haloExtractKernel = source->haloExtractKernel;
  target->addScalarKernel = source->addScalarKernel;
  target->maskKernel = source->maskKernel;
  target->sumKernel = source->sumKernel;
#endif
}
mesh_t* create_extended_mesh(mesh_t* mesh){
  mesh_t *fdm_mesh = new mesh_t();
  copy_mesh(fdm_mesh,mesh);
  auto nq = mesh->Nq;
  auto np_fdm = (nq+2)*(nq+2)*(nq+2);
  dlong Ntotal_fdm = np_fdm*mesh->Nelements;
  meshLoadReferenceNodesHex3D(fdm_mesh, mesh->N+2);
  meshPhysicalNodesHex3D(fdm_mesh);
  meshGeometricFactorsHex3D(fdm_mesh);
  meshConnectFaceNodes3D(fdm_mesh);
  meshParallelConnectNodes(fdm_mesh);
  auto ogs = ogsSetup(Ntotal_fdm, fdm_mesh->globalIds, fdm_mesh->comm, 1, fdm_mesh->device);
  fdm_mesh->ogs = ogs;
  return fdm_mesh;
}
FDM_Operator::FDM_Operator(int dim_, int num_elements_, int N_, mesh_t* mesh_):
    dim(dim_),
    num_elements(num_elements_),
    N(N_),
    operator_size(N_ * N_),
    mesh(mesh_),
    extended_mesh(create_extended_mesh(mesh_))
{
    S_x = mesh->device.malloc<dfloat>(operator_size * num_elements);
    S_y = mesh->device.malloc<dfloat>(operator_size * num_elements);
    S_z = mesh->device.malloc<dfloat>(operator_size * num_elements);

    num_points_elem = (dim == 2) ? N * N : N * N * N;
    num_points_total = num_elements * num_points_elem;
    auto nm2 = N-2;

    inv_L = mesh->device.malloc<dfloat>(num_points_total);
    weights = mesh->device.malloc<dfloat>(nm2*nm2*4*3*num_elements);
    work_dev_1 = mesh->device.malloc<dfloat>(num_points_total);
    work_dev_2 = mesh->device.malloc<dfloat>(num_points_total);

    // Kernels
    // set kernel name suffix
    char fileName[BUFSIZ], kernelName[BUFSIZ];
    properties += mesh->device.properties();
    properties["defines/DIM"] = dim;
    properties["defines/N"] = N;
    properties["defines/N2"] = N * N;
    properties["defines/NUM_ELEMENTS"] = num_elements;
    properties["defines/NUM_POINTS_TOTAL"] = num_points_total;
    properties["defines/NUM_POINTS_ELEM"] = num_points_elem;

    sprintf(fileName, DELLIPTIC "/okl/fdm_operator.okl");
    fdm_kernel = device.buildKernel(fileName, "fdm_operator", properties);
    zero_all = device.buildKernel(fileName, "zero_all", properties);
    apply_weighting = device.buildKernel(fileName, "hsmg_schwarz_wt", properties);
    hsmg_schwarz_toext = device.buildKernel(fileName, "hsmg_schwarz_toext", properties);
    extrude_kernel = device.buildKernel(fileName, "extrude", properties);
    hsmg_schwarz_toreg = device.buildKernel(fileName, "hsmg_schwarz_toreg", properties);
    data_motion_op1 = device.buildKernel(fileName, "data_motion_op1", properties);
    data_motion_op2 = device.buildKernel(fileName, "data_motion_op2", properties);
}
void FDM_Operator::apply(occa::memory Su, occa::memory u)
{
    zero_all(work_dev_1,work_dev_2);
    hsmg_schwarz_toext(work_dev_1,u);
    extrude_kernel(work_dev_1,0,0.0,work_dev_1,2,1.0);
    ogsGatherScatter(work_dev_1, ogsDfloat, ogsAdd, extended_mesh->ogs);
    extrude_kernel(work_dev_1,0,1.0,work_dev_1,2,-1.0);
    fdm_kernel(work_dev_2,S_x, S_y, S_z, inv_L, work_dev_1);
    extrude_kernel(work_dev_1,0,0.0,work_dev_2,0,1.0);
    ogsGatherScatter(work_dev_2, ogsDfloat, ogsAdd, extended_mesh->ogs);
    extrude_kernel(work_dev_2,0,1.0,work_dev_1,0,-1.0);
    extrude_kernel(work_dev_2,2,1.0,work_dev_2,0,1.0);
    hsmg_schwarz_toreg(Su, work_dev_2);
    ogsGatherScatter(Su, ogsDfloat, ogsAdd, mesh->ogs);
    apply_weighting(Su,weights);
}
void FDM_Operator::build(
            double* Sx,
            double* Sy,
            double* Sz,
            double* D,
            double* wts)
{
  S_x.copyFrom(Sx, operator_size * num_elements * sizeof(double));
  S_y.copyFrom(Sy, operator_size * num_elements * sizeof(double));
  S_z.copyFrom(Sz, operator_size * num_elements * sizeof(double));
  inv_L.copyFrom(D, num_points_total * sizeof(double));
  int weightSize = (N-2)*(N-2)*4*3*num_elements;
  weights.copyFrom(wts, weightSize * sizeof(double));
}
FDM_Operator::~FDM_Operator(){}
void MGLevel::Ax(occa::memory o_x, occa::memory o_Ax) {
  ellipticOperator(elliptic,o_x,o_Ax, dfloatString); // "float" ); // hard coded for testing (should make an option)
}

void MGLevel::residual(occa::memory o_rhs, occa::memory o_x, occa::memory o_res) {
  ellipticOperator(elliptic,o_x,o_res, dfloatString); // "float" ); // hard coded for testing (should make an option)

  // subtract r = b - A*x
  ellipticScaledAdd(elliptic, 1.f, o_rhs, -1.f, o_res);
}

void MGLevel::coarsen(occa::memory o_x, occa::memory o_Rx) {
  if (options.compareArgs("DISCRETIZATION","CONTINUOUS"))
    elliptic->dotMultiplyKernel(mesh->Nelements*NpF, o_invDegree, o_x, o_x);

  elliptic->precon->coarsenKernel(mesh->Nelements, o_R, o_x, o_Rx);

  if (options.compareArgs("DISCRETIZATION","CONTINUOUS")) {
    ogsGatherScatter(o_Rx, ogsDfloat, ogsAdd, elliptic->ogs);
    if (elliptic->Nmasked) mesh->maskKernel(elliptic->Nmasked, elliptic->o_maskIds, o_Rx);
  }
}

void MGLevel::prolongate(occa::memory o_x, occa::memory o_Px) {
  elliptic->precon->prolongateKernel(mesh->Nelements, o_R, o_x, o_Px);
}

void MGLevel::smooth(occa::memory o_rhs, occa::memory o_x, bool x_is_zero) {
  if (stype==RICHARDSON) {
    this->smoothRichardson(o_rhs, o_x, x_is_zero);
  } else if (stype==CHEBYSHEV) {
    this->smoothChebyshev(o_rhs, o_x, x_is_zero);
  } else if (stype==SCHWARZ) {
    this->smoothSchwarz(o_rhs, o_x, x_is_zero);
  }
}

void MGLevel::smoother(occa::memory o_x, occa::memory o_Sx) {
  if (smtype==JACOBI) {
    this->smootherJacobi(o_x, o_Sx);
  } else if (smtype==LOCALPATCH) {
    this->smootherLocalPatch(o_x, o_Sx);
  }
}

void MGLevel::smoothRichardson(occa::memory &o_r, occa::memory &o_x, bool xIsZero) {

  occa::memory o_res = o_smootherResidual;

  if (xIsZero) {
    this->smoother(o_r, o_x);
    return;
  }

  dfloat one = 1.; dfloat mone = -1.;

  //res = r-Ax
  this->Ax(o_x,o_res);
  elliptic->scaledAddKernel(Nrows, one, o_r, mone, o_res);

  //smooth the fine problem x = x + S(r-Ax)
  this->smoother(o_res, o_res);
  elliptic->scaledAddKernel(Nrows, one, o_res, one, o_x);
}

void MGLevel::smoothChebyshev (occa::memory &o_r, occa::memory &o_x, bool xIsZero) {

  const dfloat theta = 0.5*(lambda1+lambda0);
  const dfloat delta = 0.5*(lambda1-lambda0);
  const dfloat invTheta = 1.0/theta;
  const dfloat sigma = theta/delta;
  dfloat rho_n = 1./sigma;
  dfloat rho_np1;

  dfloat one = 1., mone = -1., zero = 0.0;

  occa::memory o_res = o_smootherResidual;
  occa::memory o_Ad  = o_smootherResidual2;
  occa::memory o_d   = o_smootherUpdate;

  if(xIsZero){ //skip the Ax if x is zero
    //res = Sr
    this->smoother(o_r, o_res);

    //d = invTheta*res
    elliptic->scaledAddKernel(Nrows, invTheta, o_res, zero, o_d);
  } else {
    //res = S(r-Ax)
    this->Ax(o_x,o_res);
    elliptic->scaledAddKernel(Nrows, one, o_r, mone, o_res);
    this->smoother(o_res, o_res);

    //d = invTheta*res
    elliptic->scaledAddKernel(Nrows, invTheta, o_res, zero, o_d);
  }

  for (int k=0;k<ChebyshevIterations;k++) {
    //x_k+1 = x_k + d_k
    if (xIsZero&&(k==0))
      elliptic->scaledAddKernel(Nrows, one, o_d, zero, o_x);
    else
      elliptic->scaledAddKernel(Nrows, one, o_d, one, o_x);

    //r_k+1 = r_k - SAd_k
    this->Ax(o_d,o_Ad);
    this->smoother(o_Ad, o_Ad);
    elliptic->scaledAddKernel(Nrows, mone, o_Ad, one, o_res);

    rho_np1 = 1.0/(2.*sigma-rho_n);
    dfloat rhoDivDelta = 2.0*rho_np1/delta;

    //d_k+1 = rho_k+1*rho_k*d_k  + 2*rho_k+1*r_k+1/delta
    elliptic->scaledAddKernel(Nrows, rhoDivDelta, o_res, rho_np1*rho_n, o_d);

    rho_n = rho_np1;
  }
  //x_k+1 = x_k + d_k
  elliptic->scaledAddKernel(Nrows, one, o_d, one, o_x);
}
void MGLevel::smoothSchwarz (occa::memory &o_r, occa::memory &o_x, bool xIsZero) {
  if (xIsZero) {
    fdm_op->apply(o_x,o_r);
    return;
  }
  return;
}

void MGLevel::smootherLocalPatch(occa::memory &o_r, occa::memory &o_Sr) {

  elliptic->precon->approxBlockJacobiSolverKernel(mesh->Nelements,
                            elliptic->precon->o_patchesIndex,
                            elliptic->precon->o_invAP,
                            elliptic->precon->o_invDegreeAP,
                            o_r,
                            o_Sr);
}

void MGLevel::smootherJacobi(occa::memory &o_r, occa::memory &o_Sr) {
  elliptic->dotMultiplyKernel(mesh->Np*mesh->Nelements,o_invDiagA,o_r,o_Sr);
}

